import java.util.Scanner;

public class ReverseApp {
    public static void main(String[] args){
        while (true){
            System.out.print("Original String : ");
            Scanner kb = new Scanner(System.in);
            String OriginSt = kb.nextLine();
            Reverser RevSt = new Reverser(OriginSt);
            String output = RevSt.doRev();
            System.out.print("Reversered String : " + output);
            System.out.println();
        }
    }
}
